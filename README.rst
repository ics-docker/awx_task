awx_task
========

ESS specific AWX_ Docker_ image.

This image includes extra Python packages.

To publish a new image, the repository should be tagged.
Always use the tag of the original awx_task image + '-ESSX' (increment X as required).

.. _Docker: https://www.docker.com
.. _AWX: https://github.com/ansible/awx
